//
//  MenuViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 01/12/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    //To get all the product details
    let dataController = DataController.sharedInstance
    
    @IBOutlet weak var sliderMenu: UIView!
    @IBOutlet weak var selectionPointer: UIView!
    @IBOutlet weak var problemsButtonView: UIButton!
    @IBOutlet weak var homeButtonView: UIButton!
    @IBOutlet weak var buyButtonView: UIButton!

    @IBOutlet weak var mainContent: UIView!
    
    @IBOutlet var menuSwipeRight: UISwipeGestureRecognizer!
    @IBOutlet var menuSwipeLeft: UISwipeGestureRecognizer!
    @IBOutlet weak var headerMenu: UIView!
    
    var problemsMainContent: UIViewController!
    var homeMainContent: UIViewController!
    var buyMainContent: UIViewController!
    
    var firstScreen: String! = "home"
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchButtonView: UIButton!
    @IBOutlet weak var cartButtonView: UIButton!
    
    //Function to show/hide search bar on click of search button with animation
    @IBAction func searchButton(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            if(self.searchBar.hidden==true) {
                self.searchBar.hidden = false
                self.searchButtonView.center.x = self.cartButtonView.center.x
                self.cartButtonView.hidden = true
                self.searchBar.frame = CGRectMake(0, 0, self.searchBar.frame.width + 38, self.searchBar.frame.height)
            }
            else {
                self.searchButtonView.center.x = self.cartButtonView.center.x - 38
                self.searchBar.frame = CGRectMake(0, 0, self.searchBar.frame.width - 38, self.searchBar.frame.height)
                self.searchBar.hidden = true
            }
        },
        completion: { finished in
            if(self.searchBar.hidden==true) {
                self.cartButtonView.hidden = false
            }
        })
    }
    
    @IBAction func problemsButton(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            self.selectionPointer.center.x = self.problemsButtonView.center.x
            self.problemsMainContent = self.storyboard!.instantiateViewControllerWithIdentifier("problemsMainContent")
            self.problemsMainContent.view.frame = CGRectMake(0, 0, self.mainContent.bounds.width, self.mainContent.bounds.height)
            self.mainContent.addSubview(self.problemsMainContent.view)
            self.firstScreen = "problems"
        })
    }
    
    @IBAction func homeButton(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            self.selectionPointer.center.x = self.homeButtonView.center.x
            self.homeMainContent = self.storyboard!.instantiateViewControllerWithIdentifier("homeMainContent")
            //print("Width \(self.mainContent.bounds.width) Height \(self.mainContent.bounds.height)")
            self.homeMainContent.view.frame = CGRectMake(0, 0, self.mainContent.bounds.width, self.mainContent.bounds.height)
            self.mainContent.addSubview(self.homeMainContent.view)
            self.firstScreen = "home"
        })
    }
    
    @IBAction func buyButton(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            self.selectionPointer.center.x = self.buyButtonView.center.x
            self.buyMainContent = self.storyboard!.instantiateViewControllerWithIdentifier("buyMainContent")
            self.buyMainContent.view.frame = CGRectMake(0, 0, self.mainContent.bounds.width, self.mainContent.bounds.height)
            self.mainContent.addSubview(self.buyMainContent.view)
            self.firstScreen = "buy"
        })
    }
    
    //action to be taken on swipe right
    @IBAction func menuSwipeRightAction(sender: UISwipeGestureRecognizer) {
        if(firstScreen=="buy") {
            homeButton(1)
        }
        else if(firstScreen=="problems") {
            //homeButton(1)
        }
        else {
            problemsButton(1)
        }
        print("Swipe Right")
    }

    //action to be taken on swipe left
    @IBAction func menuSwipeLeftAction(sender: UISwipeGestureRecognizer) {
        if(firstScreen=="buy") {
            //buyButton(1)
        }
        else if(firstScreen=="problems") {
            homeButton(1)
        }
        else {
            buyButton(1)
        }
        print("Swipe Left")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //To set the height and position of search bar on load
        self.searchBar.frame = CGRectMake(0, 0, self.searchBar.frame.width, self.searchBar.frame.height-4)
        
        //Add shadow to main header
        addShadowToHeader()
    }
    
    override func viewDidAppear(animated: Bool) {
        //Initially set the pointer to the requested tab or set it to home by default
        if(firstScreen=="buy") {
            buyButton(1)
        }
        else if(firstScreen=="problems") {
            problemsButton(1)
        }
        else {
            homeButton(1)
        }
        
        //Create gesture for swipe left
        menuSwipeLeft = UISwipeGestureRecognizer(target:self, action: "menuSwipeLeftAction:")
        sliderMenu.addGestureRecognizer(menuSwipeLeft)
        menuSwipeLeft.direction = .Left
        //Create gesture for swipe right
        menuSwipeRight = UISwipeGestureRecognizer(target:self, action: "menuSwipeRightAction:")
        sliderMenu.addGestureRecognizer(menuSwipeRight)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Change status bar text to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    //Add shadow to main header
    func addShadowToHeader() {
        self.headerMenu.layer.shadowColor = UIColor(white: 0.3, alpha: 1.0).CGColor
        self.headerMenu.layer.shadowOffset = CGSizeMake(0, 1)
        self.headerMenu.layer.shadowOpacity = 0.7
        self.headerMenu.layer.shadowRadius = 0.3
        self.headerMenu.layer.masksToBounds = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
