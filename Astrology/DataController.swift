//
//  DataController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 23/12/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit
import CoreData

class DataController: UIViewController {
    
    static let sharedInstance = DataController()
    
    /*
    //Create json here later
    struct productJson {
    let productImageName = ["PID1","PID2","PID1", "PID2", "PID1","PID2","PID1", "PID2","PID1", "PID2"]
    let productName = ["Product1", "Product2", "Product3", "Product4", "Product5", "Product6", "Product7", "Product8","Product9", "Product10"]
    let productsCategory = [0, 0, 1, 1, 2, 2, 2, 3, 4, 2]
    let productNewPrice = [100, 200, 300, 400, 500, 350, 120, 250, 300, 50]
    let productOldPrice = [110, 210, 320, 420, 490, 350, 120, 240, 290, 45]
    }*/
    //Get categories list here
    var categories = ["HEALTH", "FAMILY", "FINANCE", "Category4", "Category5"]
    //Get product name and images from backend according to category
    let productImageName = ["PID1","PID2","PID1", "PID2", "PID1","PID2","PID1", "PID2","PID1", "PID2"]
    let productImages = [["PID1","PID2", "PID1"], ["PID2", "PID1", "PID1", "PID2"], ["PID1", "PID2"], ["PID1","PID2"], ["PID1", "PID1"], ["PID2", "PID2"], ["PID1", "PID1"], ["PID1", "PID1"], ["PID2", "PID2"], ["PID1", "PID1"]]
    let productName = ["Product1", "Product2", "Product3", "Product4", "Product5", "Product6", "Product7", "Product8","Product9", "Product10"]
    let productsCategory = [0, 0, 1, 1, 2, 2, 2, 3, 3, 4]
    let productNewPrice = [100, 200, 300, 400, 500, 350, 120, 250, 300, 50]
    let productOldPrice = [110, 210, 320, 420, 700, 350, 120, 500, 400, 60]
    let productRating = [2, 1, 3, 4, 5, 0, 1, 3, 4, 2]
    let productQuantity = [0, 1, 0, 5, 10, 14, 4, 6, 8, 19]
    let productDescription = [
        "Description about product 1 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 2 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 3 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 4 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 5 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 6 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 7 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 8 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 9 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "Description about product 10 - Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."]
    let currency = "Rs. "
    
    let placesIndia = ["Select Place","Achalpur","Achhnera","Adalaj","Adilabad","Adityapur","Adoni","Adoor","Adra","Adyar","Afzalpur","Agartala","Agra","Ahmedabad","Ahmednagar","Aizawl","Ajmer","Akola","Akot","Alappuzha","Aligarh","AlipurdUrban Agglomerationr","Alirajpur","Allahabad","Alwar","Amalapuram","Amalner","Ambejogai","Ambikapur","Amravati","Amreli","Amritsar","Amroha","Anakapalle","Anand","Anantapur","Anantnag","Anjangaon","Anjar","Ankleshwar","Arakkonam","Arambagh","Araria","Arrah","Arsikere","Aruppukkottai","Arvi","Arwal","Asansol","Asarganj","Ashok Nagar","Athni","Attingal","Aurangabad","Aurangabad","Azamgarh","Bagaha","Bageshwar","Bahadurgarh","Baharampur","Bahraich","Balaghat","Balangir","Baleshwar Town","Ballari","Balurghat","Bankura","Bapatla","Baramula","Barbil","Bargarh","Barh","Baripada Town","Barmer","Barnala","Barpeta","Batala","Bathinda","Begusarai","Belagavi","Bellampalle","Belonia","Bengaluru","Bettiah","BhabUrban Agglomeration","Bhadrachalam","Bhadrak","Bhagalpur","Bhainsa","Bharatpur","Bharuch","Bhatapara","Bhavnagar","Bhawanipatna","Bheemunipatnam","Bhilai Nagar","Bhilwara","Bhimavaram","Bhiwandi","Bhiwani","Bhongir","Bhopal","Bhubaneswar","Bhuj","Bikaner","Bilaspur","Bobbili","Bodhan","Bokaro Steel City","Bongaigaon City","Brahmapur","Buxar","Byasanagar","Chaibasa","Chalakudy","Chandausi","Chandigarh","Changanassery","Charkhi Dadri","Chatra","Chennai","Cherthala","Chhapra","Chhapra","Chikkamagaluru","Chilakaluripet","Chirala","Chirkunda","Chirmiri","Chittoor","Chittur-Thathamangalam","Coimbatore","Cuttack","Dalli-Rajhara","Darbhanga","Darjiling","Davanagere","Deesa","Dehradun","Dehri-on-Sone","Delhi","Deoghar","Dhamtari","Dhanbad","Dharmanagar","Dharmavaram","Dhenkanal","Dhoraji","Dhubri","Dhule","Dhuri","Dibrugarh","Dimapur","Diphu","Dumka","Dumraon","Durg","Eluru","English Bazar","Erode","Etawah","Faridabad","Faridkot","Farooqnagar","Fatehabad","Fatehpur Sikri","Fazilka","Firozabad","Firozpur","Firozpur Cantt.","Forbesganj","Gadwal","Gangarampur","Ganjbasoda","Gaya","Giridih","Goalpara","Gobichettipalayam","Gobindgarh","Godhra","Gohana","Gokak","Gooty","Gopalganj","Gudivada","Gudur","Gumia","Guntakal","Guntur","Gurdaspur","Gurgaon","Guruvayoor","Guwahati","Gwalior","Habra","Hajipur","Haldwani-cum-Kathgodam","Hansi","Hapur","Hardoi","Hardwar","Hazaribag","Hindupur","Hisar","Hoshiarpur","Hubli-Dharwad","Hugli-Chinsurah","Hyderabad","Ichalkaranji","Imphal","Indore","Itarsi","Jabalpur","Jagdalpur","Jaggaiahpet","Jagraon","Jagtial","Jaipur","Jalandhar","Jalandhar Cantt.","Jalpaiguri","Jamalpur","Jammalamadugu","Jammu","Jamnagar","Jamshedpur","Jamui","Jangaon","Jatani","Jehanabad","Jhansi","Jhargram","Jharsuguda","Jhumri Tilaiya","Jind","Jodhpur","Jorhat","Kadapa","Kadi","Kadiri","Kagaznagar","Kailasahar","Kaithal","Kakinada","Kalimpong","Kalpi","Kalyan-Dombivali","Kamareddy","Kancheepuram","Kandukur","Kanhangad","Kannur","Kanpur","Kapadvanj","Kapurthala","Karaikal","Karimganj","Karimnagar","Karjat","Karnal","Karur","Karwar","Kasaragod","Kashipur","KathUrban Agglomeration","Katihar","Kavali","Kayamkulam","Kendrapara","Kendujhar","Keshod","Khair","Khambhat","Khammam","Khanna","Kharagpur","Kharar","Khowai","Kishanganj","Kochi","Kodungallur","Kohima","Kolar","Kolkata","Kollam","Koratla","Korba","Kothagudem","Kot Kapura","Kottayam","Kovvur","Koyilandy","Kozhikode","Kunnamkulam","Kurnool","Kyathampalle","Lachhmangarh","Ladnu","Ladwa","Lahar","Laharpur","Lakheri","Lakhimpur","Lakhisarai","Lakshmeshwar","Lalganj","Lalganj","Lal Gopalganj Nindaura","Lalgudi","Lalitpur","Lalsot","Lanka","Lar","Lathi","Latur","Lilong","Limbdi","Lingsugur","Loha","Lohardaga","Lonar","Lonavla","Longowal","Loni","Losal","Lucknow","Ludhiana","Lumding","Lunawada","Lunglei","Macherla","Machilipatnam","Madanapalle","Maddur","Madhepura","Madhubani","Madhugiri","Madhupur","Madikeri","Madurai","Magadi","Mahad","Mahalingapura","Maharajganj","Maharajpur","Mahasamund","Mahbubnagar","Mahe","Mahemdabad","Mahendragarh","Mahesana","Mahidpur","Mahnar Bazar","Mahuva","Maihar","Mainaguri","Makhdumpur","Makrana","Malaj Khand","Malappuram","Malavalli","Malda","Malegaon","Malerkotla","Malkangiri","Malkapur","Malout","Malpura","Malur","Manachanallur","Manasa","Manavadar","Manawar","Mancherial","Mandalgarh","Mandamarri","Mandapeta","Mandawa","Mandi","Mandi Dabwali","Mandideep","Mandla","Mandsaur","Mandvi","Mandya","Manendragarh","Maner","Mangaldoi","Mangaluru","Mangalvedhe","Manglaur","Mangrol","Mangrol","Mangrulpir","Manihari","Manjlegaon","Mankachar","Manmad","Mansa","Mansa","Manuguru","Manvi","Manwath","Mapusa","Margao","Margherita","Marhaura","Mariani","Marigaon","Markapur","Marmagao","Masaurhi","Mathabhanga","Mathura","Mattannur","Mauganj","Mavelikkara","Mavoor","Mayang Imphal","Medak","Medininagar (Daltonganj)","Medinipur","Meerut","Mehkar","Memari","Merta City","Mhaswad","Mhow Cantonment","Mhowgaon","Mihijam","Mira-Bhayandar","Mirganj","Miryalaguda","Modasa","Modinagar","Moga","Mohali","Mokameh","Mokokchung","Monoharpur","Moradabad","Morena","Morinda India","Morshi","Morvi","Motihari","Motipur","Mount Abu","Mudabidri","Mudalagi","Muddebihal","Mudhol","Mukerian","Mukhed","Muktsar","Mul","Mulbagal","Multai","Mumbai","Mundargi","Mundi","Mungeli","Munger","Murliganj","Murshidabad","Murtijapur","Murwara (Katni)","Musabani","Mussoorie","Muvattupuzha","Muzaffarpur","Mysore","Nabadwip","Nabarangapur","Nabha","Nadbai","Nadiad","Nagaon","Nagapattinam","Nagar","Nagari","Nagarkurnool","Nagaur","Nagda","Nagercoil","Nagina","Nagla","Nagpur","Nahan","Naharlagun","Naidupet","Naihati","Naila Janjgir","Nainital","Nainpur","Najibabad","Nakodar","Nakur","Nalbari","Namagiripettai","Namakkal","Nanded-Waghala","Nandgaon","Nandivaram-Guduvancheri","Nandura","Nandurbar","Nandyal","Nangal","Nanjangud","Nanjikottai","Nanpara","Narasapuram","Narasaraopet","Naraura","Narayanpet","Nargund","Narkatiaganj","Narkhed","Narnaul","Narsinghgarh","Narsinghgarh","Narsipatnam","Narwana","Nashik","Nasirabad","Natham","Nathdwara","Naugachhia","Naugawan Sadat","Nautanwa","Navalgund","Navsari","Nawabganj","Nawada","Nawanshahr","Nawapur","Nedumangad","Neem-Ka-Thana","Neemuch","Nehtaur","Nelamangala","Nellikuppam","Nellore","Nepanagar","New Delhi","Neyveli (TS)","Neyyattinkara","Nidadavole","Nilambur","Nilanga","Nimbahera","Nirmal","Niwai","Niwari","Nizamabad","Nohar","Noida","Nokha","Nokha","Nongstoin","Noorpur","North Lakhimpur","Nowgong","Nowrozabad (Khodargama)","Nuzvid","O' Valley","Obra","Oddanchatram","Ongole","Orai","Osmanabad","Ottappalam","Ozar","P.N.Patti","Pachora","Pachore","Pacode","Padmanabhapuram","Padra","Padrauna","Paithan","Pakaur","Palacole","Palai","Palakkad","Palampur","Palani","Palanpur","Palasa Kasibugga","Palghar","Pali","Pali","Palia Kalan","Palitana","Palladam","Pallapatti","Pallikonda","Palwal","Palwancha","Panagar","Panagudi","Panaji","Panamattom","Panchkula","Panchla","Pandharkaoda","Pandharpur","Pandhurna","PandUrban Agglomeration","Panipat","Panna","Panniyannur","Panruti","Panvel","Pappinisseri","Paradip","Paramakudi","Parangipettai","Parasi","Paravoor","Parbhani","Pardi","Parlakhemundi","Parli","Partur","Parvathipuram","Pasan","Paschim Punropara","Pasighat","Patan","Pathanamthitta","Pathankot","Pathardi","Pathri","Patiala","Patna","Patratu","Pattamundai","Patti","Pattran","Pattukkottai","Patur","Pauni","Pauri","Pavagada","Pedana","Peddapuram","Pehowa","Pen","Perambalur","Peravurani","Peringathur","Perinthalmanna","Periyakulam","Periyasemur","Pernampattu","Perumbavoor","Petlad","Phagwara","Phalodi","Phaltan","Phillaur","Phulabani","Phulera","Phulpur","Phusro","Pihani","Pilani","Pilibanga","Pilibhit","Pilkhuwa","Pindwara","Pinjore","Pipar City","Pipariya","Piriyapatna","Piro","Pithampur","Pithapuram","Pithoragarh","Pollachi","Polur","Pondicherry","Ponnani","Ponneri","Ponnur","Porbandar","Porsa","Port Blair","Powayan","Prantij","Pratapgarh","Pratapgarh","Prithvipur","Proddatur","Pudukkottai","Pudupattinam","Pukhrayan","Pulgaon","Puliyankudi","Punalur","Punch","Pune","Punganur","Punjaipugalur","Puranpur","Puri","Purna","Purnia","PurqUrban Agglomerationzi","Purulia","Purwa","Pusad","Puthuppally","Puttur","Puttur","Qadian","Raayachuru","Rabkavi Banhatti","Radhanpur","Rae Bareli","Rafiganj","Raghogarh-Vijaypur","Raghunathganj","Raghunathpur","Rahatgarh","Rahuri","Raiganj","Raigarh","Raikot","Raipur","Rairangpur","Raisen","Raisinghnagar","Rajagangapur","Rajahmundry","Rajakhera","Rajaldesar","Rajam","Rajampet","Rajapalayam","Rajauri","Rajgarh","Rajgarh (Alwar)","Rajgarh (Churu)","Rajgir","Rajkot","Rajnandgaon","Rajpipla","Rajpura","Rajsamand","Rajula","Rajura","Ramachandrapuram","Ramagundam","Ramanagaram","Ramanathapuram","Ramdurg","Rameshwaram","Ramganj Mandi","Ramgarh","Ramnagar","Ramnagar","Ramngarh","Rampur","Rampura Phul","Rampurhat","Rampur Maniharan","Ramtek","Ranaghat","Ranavav","Ranchi","Ranebennuru","Rangia","Rania","Ranibennur","Ranipet","Rapar","Rasipuram","Rasra","Ratangarh","Rath","Ratia","Ratlam","Ratnagiri","Rau","Raurkela","Raver","Rawatbhata","Rawatsar","Raxaul Bazar","Rayachoti","Rayadurg","Rayagada","Reengus","Rehli","Renigunta","Renukoot","Reoti","Repalle","Revelganj","Rewa","Rewari","Rishikesh","Risod","Robertsganj","Robertson Pet","Rohtak","Ron","Roorkee","Rosera","Rudauli","Rudrapur","Rudrapur","Rupnagar","Sabalgarh","Sadabad","Sadalagi","Sadasivpet","Sadri","Sadulpur","Sadulshahar","Safidon","Safipur","Sagar","Sagara","Sagwara","Saharanpur","Saharsa","Sahaspur","Sahaswan","Sahawar","Sahibganj","Sahjanwa","Saidpur","Saiha","Sailu","Sainthia","Sakaleshapura","Sakti","Salaya","Salem","Salur","Samalkha","Samalkot","Samana","Samastipur","Sambalpur","Sambhal","Sambhar","Samdhan","Samthar","Sanand","Sanawad","Sanchore","Sandi","Sandila","Sanduru","Sangamner","Sangareddy","Sangaria","Sangli","Sangole","Sangrur","Sankarankovil","Sankari","Sankeshwara","Santipur","Sarangpur","Sardarshahar","Sardhana","Sarni","Sarsod","Sasaram","Sasvad","Satana","Satara","Sathyamangalam","Satna","Sattenapalle","Sattur","Saunda","Saundatti-Yellamma","Sausar","Savanur","Savarkundla","Savner","Sawai Madhopur","Sawantwadi","Sedam","Sehore","Sendhwa","Seohara","Seoni","Seoni-Malwa","Shahabad","Shahabad Hardoi","Shahabad Rampur","Shahade","Shahbad","Shahdol","Shahganj","Shahjahanpur","Shahpur","Shahpura","Shahpura","Shajapur","Shamgarh","Shamli","Shamsabad Agra","Shamsabad Farrukhabad","Shegaon","Sheikhpura","Shendurjana","Shenkottai","Sheoganj","Sheohar","Sheopur","Sherghati","Sherkot","Shiggaon","Shikaripur","Shikarpur Bulandshahr","Shikohabad","Shillong","Shimla","Shirdi","Shirpur-Warwade","Shirur","Shishgarh","Shivamogga","Shivpuri","Sholavandan","Sholingur","Shoranur","Shrigonda","Shrirampur","Shrirangapattana","Shujalpur","Siana","Sibsagar","Siddipet","Sidhi","Sidhpur","Sidlaghatta","Sihor","Sihora","Sikanderpur","Sikandrabad","Sikandra Rao","Sikar","Silao","Silapathar","Silchar","Siliguri","Sillod","Silvassa","Simdega","Sindagi","Sindhagi","Sindhnur","Singrauli","Sinnar","Sira","Sircilla","Sirhind Fatehgarh Sahib","Sirkali","Sirohi","Sironj","Sirsa","Sirsaganj","Sirsi","Sirsi","Siruguppa","Sitamarhi","Sitapur","Sitarganj","Sivaganga","Sivagiri","Sivakasi","Siwan","Sohagpur","Sohna","Sojat","Solan","Solapur","Sonamukhi","Sonepur","Songadh","Sonipat","Sopore","Soro","Soron","Soyagaon","Srikakulam","Srikalahasti","Sri Madhopur","Srinagar","Srinagar","Srinivaspur","Srirampore","Srisailam Project (Right Flank Colony) Township","Srivilliputhur","Sugauli","Sujangarh","Sujanpur","Sullurpeta","Sultanganj","Sultanpur","Sumerpur","Sumerpur","Sunabeda","Sunam","Sundargarh","Sundarnagar","Supaul","Surandai","Surapura","Surat","Suratgarh","SUrban Agglomerationr","Suri","Suriyampalayam","Suryapet","Tadepalligudem","Tadpatri","Takhatgarh","Taki","Talaja","Talcher","Talegaon Dabhade","Talikota","Taliparamba","Talode","Talwara","Tamluk","Tanda","Tandur","Tanuku","Tarakeswar","Tarana","Taranagar","Taraori","Tarbha","Tarikere","Tarn Taran","Tasgaon","Tehri","Tekkalakote","Tenali","Tenkasi","Tenu dam-cum-Kathhara","Terdal","Tezpur","Thakurdwara","Thammampatti","Thana Bhawan","Thane","Thanesar","Thangadh","Thanjavur","Tharad","Tharamangalam","Tharangambadi","Theni Allinagaram","Thirumangalam","Thirupuvanam","Thiruthuraipoondi","Thiruvalla","Thiruvallur","Thiruvananthapuram","Thiruvarur","Thodupuzha","Thoubal","Thrissur","Thuraiyur","Tikamgarh","Tilda Newra","Tilhar","Tindivanam","Tinsukia","Tiptur","Tirora","Tiruchendur","Tiruchengode","Tiruchirappalli","Tirukalukundram","Tirukkoyilur","Tirunelveli","Tirupathur","Tirupathur","Tirupati","Tiruppur","Tirur","Tiruttani","Tiruvannamalai","Tiruvethipuram","Tiruvuru","Tirwaganj","Titlagarh","Tittakudi","Todabhim","Todaraisingh","Tohana","Tonk","Tuensang","Tuljapur","Tulsipur","Tumkur","Tumsar","Tundla","Tuni","Tura","Uchgaon","Udaipur","Udaipur","Udaipurwati","Udgir","Udhagamandalam","Udhampur","Udumalaipettai","Udupi","Ujhani","Ujjain","Umarga","Umaria","Umarkhed","Umbergaon","Umred","Umreth","Una","Unjha","Unnamalaikadai","Unnao","Upleta","Uran","Uran Islampur","Uravakonda","Urmar Tanda","Usilampatti","Uthamapalayam","Uthiramerur","Utraula","Vadakkuvalliyur","Vadalur","Vadgaon Kasba","Vadipatti","Vadnagar","Vadodara","Vaijapur","Vaikom","Valparai","Valsad","Vandavasi","Vaniyambadi","Vapi","Vapi","Varanasi","Varkala","Vasai-Virar","Vatakara","Vedaranyam","Vellakoil","Vellore","Venkatagiri","Veraval","Vidisha","Vijainagar Ajmer","Vijapur","Vijayapura","Vijayawada","Vijaypur","Vikarabad","Vikramasingapuram","Viluppuram","Vinukonda","Viramgam","Virudhachalam","Virudhunagar","Visakhapatnam","Visnagar","Viswanatham","Vita","Vizianagaram","Vrindavan","Vyara","Wadgaon Road","Wadhwan","Wadi","Wai","Wanaparthy","Wani","Wankaner","Warangal","Wara Seoni","Wardha","Warhapur","Warisaliganj","Warora","Warud","Washim","Wokha","Yadgir","Yamunanagar","Yanam","Yavatmal","Yawal","Yellandu","Yemmiganur","Yerraguntla","Yevla","Zaidpur","Zamania","Zira","Zirakpur","Zunheboto"]
    

    var birthDate: String!
    var birthTime: String!
    var birthPlace: String!
    
    // main URL for server
    let mainURL = "https://polar-headland-2900.herokuapp.com"

    // blank homePageCells
    var homePageCells = [
//            [
//                    "title":"Sample Card",
//                    "text":"Enter Birth Details to View Your Personal Details",
//                    "image":"donate"
//            ]
    ] as [[String:AnyObject]]
    //check if date has changed
    var dateChanged = false

//    var homeCellTitleNames = [
//        "planet_status": "Planet Status",
//        "rinn_pitri": "Rinn Pitri",
//        "remedy_list": "Remedy List",
//        "about_education_and_occupation": "Education & Occupation",
//        "life_head": "Life Head",
//        "disease": "Deseases",
//        "married_life": "Married Life",
//        "favoured_god": "Favoured God",
//        "important_information": "Important Info",
//        "nature": "Nature",
//        "mix_mahadasha_current": "Mahadasha Current",
//        "category": "Category",
//        "occupation":  "Occupation",
//        "mix_mahadasha_not_current": "Mahadasha",
//        "santan": "Santan",
//        "not_to_donate": "Not to Donate",
//        "color_not_to_wear": "Color Not To Wear",
//        "about_kundali": "About Kundali",
//        "donate": "Donate"
//    ]
    
    // use this function for all server requests
    func makeServerRequestWith(
        url: NSURL,
        data: NSData,
        method: String,
        callBack:(
            response: NSURLResponse!,
            data: NSData!,
            error: NSError!
        )
        -> Void){
            
        // server request function
        // params: url, data, method, callback func
        
        //#************************* Network Method ****************************************************
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //      ********************************************  ********************************************
        
        //Initiate session to get data from server
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request) {
            (data, response, error) -> Void in
            if error != nil {
                callBack(response: nil, data: data, error: error)
            }
            else {
                callBack(response: response, data: data, error: nil)
            }
        }
        task.resume()
    }

    func getBasicPrediction(
        callBack:(status: Bool)// after we get the data this will be called
            -> Void){
                
        var endPoint = "/frm"
                
        var dateArr = self.birthDate!.characters.split("-")
        var timeArr = self.birthTime!.characters.split(":")
                
        endPoint += "/" + String(dateArr[0])
        endPoint += "/" + String(dateArr[1])
        endPoint += "/" + String(dateArr[2])
        endPoint += "/" + String(timeArr[0])
        endPoint += "/" + String(timeArr[1])
                
        print("End Point: \(endPoint)")
                
        let url = NSURL(string: self.mainURL + endPoint)
        print("Url generated is \(url)")

        var homePageCellSample = [String:AnyObject]()
     
        self.makeServerRequestWith(
            url!,
            data: NSData(),
            method: "GET",
            callBack: {
                // callBack Function
                (response, data, error) in
                
                if((response as! NSHTTPURLResponse).statusCode == 200){
                    print("Got basic details. Response Code: \(response)") // NSString(data: data!, encoding: NSUTF8StringEncoding))
                    do {
                         /* get your json data */
                        if let jsonDict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? NSDictionary {
                            print("NSDict is : \(jsonDict)")
                            
                            //Set home page cells as nil before getting data so data does not repeat itself
                            self.homePageCells = []
                            for (key, var value) in jsonDict{
                                
                                print("Doing Key: \(key)")

                                    if (
                                        (value as? String) != ""
                                            &&
                                        (key as? String) != ""
                                        ){
                                        
                                        if (key as? String == "remedy_list"){
                                            
                                            var tempText = ""
                                            
                                            for (val) in value as! NSArray{
                                                //(String(val["number"] as! Int)) + " : " + 
                                                tempText += (val["description"] as! String) + " \n"
                                                
                                            }
                                            
                                            value = tempText
                                            
                                        }
                                        
                                        if ( key as? String == "about_kundali"){
                                            
                                            var tempText = ""
                                            
                                            for (key, val) in value as! NSDictionary{
                                                tempText += (key as! String) + " : " + (val as! String) + " \n"
                                                
                                            }
                                            
                                            value = tempText
                                        }
                                        
                                        if(key as? String == "planet_status"){
                                            
                                            var tempText = ""
                                            
                                            for val in value as! NSArray{
                                                
                                                tempText += (val["planet"] as! String) + " : " + (val["status"] as! String) + "\n"
                                                
                                            }
                                            
                                            value = tempText
                                            
                                        }
                                            
                                        if(key as? String != "code"){
                                            homePageCellSample = [
                                                "title": key,//self.homeCellTitleNames[key as! String]!,
                                                "text": value,
                                                "image": key
                                                ]
                                            print("Added :")// \(homePageCellSample)")
                                            self.homePageCells.append(homePageCellSample)
                                        }
                                    }
                            }
                            
                            // go back to where we belong
                            callBack(status: true)
                        }
                    } catch let error as NSError {
                        
                        print("Error in Converting to JSON.s Error: \(error.localizedDescription)")
                        callBack(status: false)
                    }

                    
                }else{
                    print("there is an error in getting basic details. Error: \(error). Response: \(response)")
                    callBack(status: false)
                }
            }
        )
        
        return
    }
    
    func getHomePageCells(
        callBack:(stat: Bool)
        -> Void){
        // Get server response
        // parse it and convert it to our format
            
//            var status = false
//            var i = 1

//            repeat{
//                print("getting home page details for \(i) th time.")
//                i++
                self.getBasicPrediction(
                    {
                        
                        (stat) in
//                        do some computaions such as
//                        incase of error redo the getting from network
//                        etc ......
//                        if stat{
//                            print("Converted to NSDictionary")
                            callBack(stat: stat)
//                        }
                    }
                    
                )
                
//            }while(not status)
            
        
        return
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
                
    }

    override func didReceiveMemoryWarning() {
        print("Got Memory Warning");
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
